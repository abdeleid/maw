<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;

class ArticleController extends Controller
{
    public function index()
    {
        $articles = Article::whenCategoryId(request()->category_id)
            ->whenSearch(request()->search)
            ->where('status', 'published')
            ->paginate(10);

        $this->incrementCategoryViews();

        $category = null;
        $otherCategories = null;

        if (request()->category_id) {

            $category = Category::FindOrFail(request()->category_id);

            $otherCategories = Category::where('id', '!=', $category->id)->limit(4)->get();

        }

        return view('articles.index', compact('category', 'articles', 'otherCategories'));

    }// end of index

    public function show(Article $article)
    {
        $this->incrementArticleViews($article);
        return view('articles.show', compact('article'));

    }// end of show

    private function incrementCategoryViews()
    {
        if (request()->category_id) {
            $category = Category::FindOrFail(request()->category_id);
            $category->increment('views_count');
        }

    }// end of incrementCategoryViews

    private function incrementArticleViews(Article $article)
    {
        $article->increment('views_count');

    }// end of incrementCategoryViews

}//end of controller
