<?php

namespace App\Http\Controllers;

use App\Models\Category;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::paginate(12);
        return view('categories.index', compact('categories'));

    }// end of index

    public function show(Category $category)
    {
        return view('categories.show', compact('category'));

    }// end of show

    public function generalOverview(Category $category)
    {
        $otherCategories = Category::where('id', '!=', $category->id)->limit(4)->get();

        return view('categories.general_overview', compact('category', 'otherCategories'));

    }// end of generalOverview

}//end of controller
