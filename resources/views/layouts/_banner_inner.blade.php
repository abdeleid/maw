<div id="banner-inner" style="{{ isset($background) ? 'background: url('  . asset($background) . ') center/cover no-repeat; background:linear-gradient(rgba(0,0,0,0.5), rgba(0,0,0,0.5)),url(' . asset($background) .') center/cover no-repeat' : '' }}">

    <nav class="navbar navbar-expand-lg navbar-dark bg-transparent">

        <div class="container">

            <a class="navbar-brand" href="{{ route('welcome') }}">@lang('site.maw')</a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">

                <ul class="navbar-nav mr-md-auto">

                    <div style="width: 600px;" class="search-form-wrapper">
                        <form action="{{ route('articles.index') }}" method="get" class="d-block d-md-flex align-items-start">
                            <input type="text" name="search" class="form-control bg-transparent mt-2 mt-md-1" placeholder="بحث في الموقع" value="{{ request()->search }}" required>
                            <button type="submit" class="btn btn-primary mx-0 mx-md-2 mt-2 mt-md-1"><i class="fa fa-search"></i> بحث</button>
                        </form>
                    </div>

                </ul>

                <ul class="navbar-nav ml-auto">
                    @auth
                        <a href="{{ route('admin.home') }}" class="nav-link">@lang('site.dashboard')</a>
                        <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="nav-link">
                            @lang('site.logout')
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    @else
                        <a href="{{ route('login') }}" class="nav-link">@lang('site.login')</a>
                    @endauth
                </ul>

            </div><!-- end of collapse -->

        </div><!-- end of container fluid-->

    </nav><!-- end of nav -->

    <div class="container">

        <div class="row justify-content-center align-items-center h-40">

            <div class="col col-md-8 mx-auto text-white text-center">

                <h2 class="banner-title">{{ $title }}</h2>

                @if (isset($body))
                    <p>{!! $body !!}</p>
                @endif

                @if (isset($category) && !request()->is('*general_overview*'))
                    <a href="{{ route('categories.general_overview', $category->id) }}" class="btn btn-primary btn-sm">@lang('categories.general_overview')</a>
                @endif

            </div><!-- end of col -->

        </div><!-- end of row -->

    </div><!-- end of container -->

</div><!-- end of banner-inner -->