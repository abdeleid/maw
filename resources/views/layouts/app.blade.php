<!doctype html>
<html lang="en" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ setting('title') }}</title>

    <meta name="title" content="{{ setting('title') }}">
    <meta name="keywords" content="{{ setting('keywords') }}">

    <meta name="description" content="{{ setting('description') }}">
    <meta name="og:url" content="{{ config('app.url') }}">
    <meta name="og:type" content="website">
    <meta name="og:title" content="{{ setting('title') }}">
    <meta name="og:description" content="{{ setting('description') }}">
    <meta name="og:image" content="{{ asset('assets/images/banner1.jpg') }}">
    <meta name="og:site_name" content="{{ setting('title') }}">

    <link rel="icon" href="{{ setting('fav_icon') ? Storage::url('uploads/' . setting('fav_icon')) : asset('assets/images/favicon.ico') }}" type="image/x-icon"/>

    <!--font awesome-->
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome5.11.2.min.css') }}">

    <!--bootstrap-->
    <!--    <link rel="stylesheet" href="dist/css/bootstrap.min.css">-->
    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.5.3/css/bootstrap.min.css" integrity="sha384-JvExCACAZcHNJEc7156QaHXTnQL3hQBixvj5RV5buE7vgnNEzzskDtx9NQ4p6BJe" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@300;400;600&display=swap" rel="stylesheet">

    <!--vendor css-->
    <link rel="stylesheet" href="{{ asset('assets/css/vendor.min.css') }}">

    <!--main styles -->
    <link rel="stylesheet" href="{{ asset('assets/css/main.min.css') }}">

    <style>
        .card {
            box-shadow: -5px 5px #eee;
        }

        .category-card-image {
            height: 198px;
        }

        @media (max-width: 750px) {
            .category-card-image {
                height: 298px;
            }

        }
    </style>

    <!--jquery-->
    <script src="{{ asset('assets/js/jquery-3.4.0.min.js') }}"></script>

    {{--noty--}}
    <link rel="stylesheet" href="{{ asset('admin_assets/plugins/noty/noty.css')}}">
    <script src="{{ asset('admin_assets/plugins/noty/noty.min.js')}}"></script>

    {{--adsense--}}
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8084523932462042" crossorigin="anonymous"></script>

    <style>
        .search-form-wrapper {
            width: 600px;
        }

        @media (max-width: 769px) {
            .search-form-wrapper {
                width: 100% !important;
            }
        }
    </style>

</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50">

@include('admin.partials._session')

@yield('content')

@include('layouts._footer')

<!--bootstrap-->
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/popper.min.js') }}"></script>

<!--vendor js-->
<script src="{{ asset('assets/js/vendor.min.js') }}"></script>

<!--main scripts-->
<script src="{{ asset('assets/js/main.min.js') }}"></script>
</body>
</html>
