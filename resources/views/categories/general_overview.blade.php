@extends('layouts.app')

@section('content')

    @include('layouts._banner_inner', ['background' => $category->image_path, 'title' => $category->title, 'body' => $category->body])
    s
    <section id="categories" class="py-5">

        <div class="container">

            <div class="row py-3">

                <div class="col-md-9">

                    {!! $category->general_overview !!}

                </div>

                <div class="col-md-3">
                    <h3>@lang('categories.other_categories')</h3>

                    @foreach ($otherCategories as $otherCategory)

                        <div class="row mb-3">

                            <div class="col-md-12">

                                <div class="card">

                                    <a href="{{ route('articles.index', ['category_id' => $otherCategory->id]) }}" class="category-card-image-wrapper">
                                        <img src="{{ $otherCategory->image_path }}" class="category-card-image img-fluid card-img-top" alt="">
                                    </a>

                                    <div class="card-body">
                                        <h5 class="mb-1">{{ $otherCategory->title }}</h5>
                                        <p style="font-size: 14px;">{!! $otherCategory->text !!}</p>
                                        <div class="d-flex justify-content-between category-info">
                                            <span style="font-size: 14px;"><i class="fa fa-file"></i> {{ $otherCategory->articles->count() }} @lang('articles.articles')</span>
                                            <span style="font-size: 14px;"><i class="fa fa-eye"></i> {{ $otherCategory->views_count }} @lang('site.views')</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    @endforeach

                    <a href="{{ route('categories.index') }}">@lang('site.show_more')</a>
                </div>

            </div><!-- end of row -->

        </div><!-- end of container -->

    </section>

@endsection