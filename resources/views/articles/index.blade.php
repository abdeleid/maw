@extends('layouts.app')

@section('content')

    @if ($category)

        @include('layouts._banner_inner', ['category' => $category, 'background' => $category->image_path, 'title' => $category->title, 'body' => $category->body])

    @else

        @include('layouts._banner_inner', ['background' => '/assets/images/categories-image.jpg', 'title' => __('site.search_results'), 'body' => __('site.search_by_using_search_bar')])

    @endif

    <section id="categories" class="py-5">

        <div class="container">

            <div class="row py-3">

                @if ($articles->count() > 0)

                    <div class="{{ $otherCategories ? 'col-md-9' : 'col-md-12' }}">

                        @foreach ($articles as $article)

                            <div class="row article">

                                <div class="col-md-3">
                                    <a href="{{ $article->file_path }}" target="_blank">
                                        <img src="{{ $article->image_path }}" class="img-fluid card-img-top mb-3" alt="">
                                    </a>
                                </div>

                                <div class="col-md-8">
                                    <h4 class="article-title">{{ $article->title }}</h4>
                                    <p class="article-text">{!! $article->body !!}</p>
                                    <div class="d-flex justify-content-between category-info">
                                        <span><i class="fab fa-readme"></i> {{ $article->time_to_read }} @lang('articles.time_to_read') </span>
                                        <span><i class="fa fa-eye"></i> {{ $article->views_count }}  @lang('site.views')</span>
                                    </div>
                                </div>

                            </div><!-- end of row -->

                        @endforeach

                        {{ $articles->appends(request()->query())->links() }}

                    </div><!-- end of col -->

                    @if ($otherCategories)

                        <div class="col-md-3">
                            <h3>@lang('categories.other_categories')</h3>

                            @foreach ($otherCategories as $otherCategory)

                                <div class="row mb-3">

                                    <div class="col-md-12">

                                        <div class="card">

                                            <a href="{{ route('articles.index', ['category_id' => $otherCategory->id]) }}" class="category-card-image-wrapper">
                                                <img src="{{ $otherCategory->image_path }}" class="category-card-image img-fluid card-img-top" alt="">
                                            </a>

                                            <div class="card-body">
                                                <h5 class="mb-1">{{ $otherCategory->title }}</h5>
                                                <p style="font-size: 14px;">{!! $otherCategory->text !!}</p>
                                                <div class="d-flex justify-content-between category-info">
                                                    <span style="font-size: 14px;"><i class="fa fa-file"></i> {{ $otherCategory->articles->count() }} @lang('articles.articles')</span>
                                                    <span style="font-size: 14px;"><i class="fa fa-eye"></i> {{ $otherCategory->views_count }} @lang('site.views')</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @endforeach

                            <a href="{{ route('categories.index') }}">@lang('site.show_more')</a>
                        </div>

                    @endif

                @else
                    <div class="col-md-12 text-center">
                        <i class="fa fa-times-circle mb-4" style="font-size: 35px;"></i>
                        <h4>@lang('site.no_data_found')</h4>
                    </div>
                @endif

            </div><!-- end of row -->

        </div><!-- end of container -->

    </section>

@endsection