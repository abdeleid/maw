<?php

return [
    'categories' => 'المواضيع',
    'category' => 'الموضوع',
    'title' => 'العنوان',
    'body' => 'المحتوى',
    'general_overview' => 'نبذه عامه',
    'image' => 'الصورة',
    'articles_count' => 'عدد المقالات',
    'other_categories' => 'مواضيع أخرى',

];