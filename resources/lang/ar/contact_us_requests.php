<?php

return [
    'contact_us_requests' => 'طلبات التواصل',
    'contact_us_request' => 'طلب تواصل',
    'name' => 'الإسم',
    'email' => 'الإيميل',
    'message' => 'الرسالة',
    'send' => 'إرسال',
];